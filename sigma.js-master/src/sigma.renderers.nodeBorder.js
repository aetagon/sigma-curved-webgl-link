;(function(undefined) {

  /**
   * Sigma Node Border Custom Renderer
   * ==================================
   *
   * The aim of this simple node renderer is to enable the user to display
   * colored node borders.
   *
   * Author: Guillaume Plique (Yomguithereal)
   * Version: 0.0.1
   */
   
  var drawIcon = function (node, x, y, size, context, threshold) {
    if(!node.icon || size < threshold) return;

    var font = node.icon.font || 'Arial',
        fgColor = node.icon.color || '#F00',
        text = node.icon.content || '?',
        px = node.icon.x || 0.5,
        py = node.icon.y || 0.5,
        height = size,
        width = size;

    var fontSizeRatio = 0.70;
    if (typeof node.icon.scale === "number") {
      fontSizeRatio = Math.abs(Math.max(0.01, node.icon.scale));
    }

    var fontSize = Math.round(fontSizeRatio * height);

    context.save();
    context.fillStyle = fgColor;

    context.font = '' + fontSize + 'px ' + font;
    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.fillText(text, x, y);
    context.restore();
  };

  sigma.canvas.nodes.circle = function(node, context, settings) {
    var prefix = settings('prefix') || '';

    context.fillStyle = node.color || settings('defaultNodeColor');
    context.beginPath();
	
    context.arc(
     node[prefix + 'x'],
     node[prefix + 'y'],
     node[prefix + 'size'],
     0,
     Math.PI * 2,
     true
    );

    context.closePath();
    context.fill();

    context.lineWidth = node.borderWidth || 0.2;
    context.strokeStyle = node.borderColor || '#fff';
    context.stroke();

    if (node.icon) {
		drawIcon(node, node[prefix + 'x'], node[prefix + 'y'], node[prefix + 'size'], context, settings('iconThreshold'));
    }
  };
  
  sigma.canvas.nodes.square = function(node, context, settings) {
    var prefix = settings('prefix') || '';

    context.fillStyle = node.color || settings('defaultNodeColor');
    context.beginPath();
	
    var rotate = Math.PI*45/180; // 45 deg rotation of a diamond shape
    context.moveTo(node[prefix + 'x']+node[prefix + 'size']*Math.sin(rotate), node[prefix + 'y']-node[prefix + 'size']*Math.cos(rotate)); // first point on outer radius, dwangle 'rotate'
    for(var i=1; i<4; i++) {
      context.lineTo(node[prefix + 'x']+Math.sin(rotate+2*Math.PI*i/4)*node[prefix + 'size'], node[prefix + 'y']-Math.cos(rotate+2*Math.PI*i/4)*node[prefix + 'size']);
    }

    context.closePath();
    context.fill();

    context.lineWidth = node.borderWidth || 0.2;
    context.strokeStyle = node.borderColor || '#fff';
    context.stroke();
	
    if (node.icon) {
		drawIcon(node, node[prefix + 'x'], node[prefix + 'y'], node[prefix + 'size'], context, settings('iconThreshold'));
    }
  };
  
  sigma.canvas.nodes.diamond = function(node, context, settings) {
    var prefix = settings('prefix') || '';

    context.fillStyle = node.color || settings('defaultNodeColor');
    context.beginPath();
	
    context.moveTo(node[prefix + 'x']-node[prefix + 'size'], node[prefix + 'y']);
    context.lineTo(node[prefix + 'x'], node[prefix + 'y']-node[prefix + 'size']);
    context.lineTo(node[prefix + 'x']+node[prefix + 'size'], node[prefix + 'y']);
    context.lineTo(node[prefix + 'x'], node[prefix + 'y']+node[prefix + 'size']);

    context.closePath();
    context.fill();

    context.lineWidth = node.borderWidth || 0.2;
    context.strokeStyle = node.borderColor || '#fff';
    context.stroke();
	
    if (node.icon) {
		drawIcon(node, node[prefix + 'x'], node[prefix + 'y'], node[prefix + 'size'], context, settings('iconThreshold'));
    }
  };
  
  sigma.canvas.nodes.equilateral = function(node, context, settings) {
    var prefix = settings('prefix') || '';

    context.fillStyle = node.color || settings('defaultNodeColor');
    context.beginPath();
	
    var pcount = (node.equilateral && node.equilateral.numPoints) || 6;
    var rotate = ((node.equilateral && node.equilateral.rotate) || 90)*Math.PI/180;
    var radius = node[prefix + 'size'];
    context.moveTo(node[prefix + 'x']+radius*Math.sin(rotate), node[prefix + 'y']-radius*Math.cos(rotate)); // first point on outer radius, angle 'rotate'
    for(var i=1; i<pcount; i++) {
      context.lineTo(node[prefix + 'x']+Math.sin(rotate+2*Math.PI*i/pcount)*radius, node[prefix + 'y']-Math.cos(rotate+2*Math.PI*i/pcount)*radius);
    }

    context.closePath();
    context.fill();

    context.lineWidth = node.borderWidth || 0.2;
    context.strokeStyle = node.borderColor || '#fff';
    context.stroke();
	
    if (node.icon) {
		drawIcon(node, node[prefix + 'x'], node[prefix + 'y'], node[prefix + 'size'], context, settings('iconThreshold'));
    }
  };
  
  sigma.canvas.nodes.star = function(node, context, settings) {
    var prefix = settings('prefix') || '';

    context.fillStyle = node.color || settings('defaultNodeColor');
    context.beginPath();
	
    var pcount = (node.star && node.star.numPoints) || 5,
        inRatio = (node.star && node.star.innerRatio) || 0.5,
        outR = node[prefix + 'size'],
        inR = node[prefix + 'size']*inRatio,
        angleOffset = Math.PI/pcount;
    context.moveTo(node[prefix + 'x'], node[prefix + 'y']-node[prefix + 'size']); // first point on outer radius, top
    for(var i=0; i<pcount; i++) {
      context.lineTo(node[prefix + 'x']+Math.sin(angleOffset+2*Math.PI*i/pcount)*inR,
          node[prefix + 'y']-Math.cos(angleOffset+2*Math.PI*i/pcount)*inR);
      context.lineTo(node[prefix + 'x']+Math.sin(2*Math.PI*(i+1)/pcount)*outR,
          node[prefix + 'y']-Math.cos(2*Math.PI*(i+1)/pcount)*outR);
    }

    context.closePath();
    context.fill();

    context.lineWidth = node.borderWidth || 0.2;
    context.strokeStyle = node.borderColor || '#fff';
    context.stroke();
	
    if (node.icon) {
		drawIcon(node, node[prefix + 'x'], node[prefix + 'y'], node[prefix + 'size'], context, settings('iconThreshold'));
    }
  };
  
  sigma.canvas.nodes.triangle = function(node, context, settings) {
    var prefix = settings('prefix') || '';

    context.fillStyle = node.color || settings('defaultNodeColor');
    context.beginPath();
	
	var x = node[prefix + 'x'];
	var y = node[prefix + 'y'];
	var size = node[prefix + 'size']
	
	context.moveTo(x, y-size);
	context.lineTo(x+Math.sin(2*Math.PI/3)*size, y-Math.cos(2*Math.PI/3)*size);
	context.lineTo(x+Math.sin(2*Math.PI*2/3)*size, y-Math.cos(2*Math.PI*2/3)*size);
	context.moveTo(x, y+size);
	
    context.closePath();
    context.fill();

    context.lineWidth = node.borderWidth || 0.2;
    context.strokeStyle = node.borderColor || '#fff';
    context.stroke();
	
    if (node.icon) {
		drawIcon(node, node[prefix + 'x'], node[prefix + 'y'], node[prefix + 'size'], context, settings('iconThreshold'));
    }
  };
  
}).call(this);