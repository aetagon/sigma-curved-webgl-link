;(function () {
    'use strict';

    sigma.utils.pkg('sigma.webgl.edges');

    /**
     * This edge renderer will display edges as lines with the gl.LINES display
     * mode. Since this mode does not support well thickness, edges are all drawn
     * with the same thickness (3px), independantly of the edge attributes or the
     * zooming ratio.
     */

    var segments = 20;
    sigma.webgl.edges.curveCPU = {
        POINTS: 6 * segments, /** #sk: segments are the divided points for the Bezier line. This will become dynamic */
        ATTRIBUTES: 4,
        addEdge: function (edge, source, target, data, i, prefix, settings) {
            var w = (edge['size'] || 1) / 2,
                x1 = source[prefix + 'x'],
                y1 = source[prefix + 'y'],
                x2 = target[prefix + 'x'],
                y2 = target[prefix + 'y'],
                color = edge.color,
                colorStart, colorEnd,
                count = edge.count || 0,
                alpha = getAlpha(color);

            if (!color) {
                switch (settings("edgeColor")) {
                    case 'mixed':
                        colorStart = source.color;
                        colorEnd = target.color;
                        // color = gradient || defaultEdgeColor;
                        break;
                    case 'source':
                        color = source.color || defaultEdgeColor;
                        break;
                    case 'target':
                        color = target.color || defaultEdgeColor;
                        break;
                    default:
                        color = defaultEdgeColor;
                        break;
                }
            }

            if (colorStart && colorEnd) {
                // Normalize star/end color:
                colorStart = sigma.utils.floatColor(colorStart);
                colorEnd = sigma.utils.floatColor(colorEnd);
            }
            else {
                // Normalize color:
                color = sigma.utils.floatColor(color);
                colorStart = colorEnd = color;
            }

            /** Setting up control point */
            var cp;
            if (source.id === target.id) {
                cp = sigma.utils.getSelfLoopControlPoints(x1 , y1, source[prefix + 'size']);
            }
            else{
                cp = sigma.utils.getQuadraticControlPoint(x1, y1, x2, y2, count);
            }

            /** This is to go in the shader */
            function getQuadraticControlPoint(x1, y1, x2, y2, a) {
                a = a || 0;
                return {
                    x: (x1 + x2) / 2 + (y2 - y1) / (60 / (15 + a)),
                    y: (y1 + y2) / 2 + (x1 - x2) / (60 / (15 + a))
                };
            };
            function addVerts(x1, y1, x2, y2, angle1, angle2, data, colorStart, colorEnd, alpha, thickness) {
                //var angle = getAngle(x1, y1, x2, y2) + (Math.PI / 2.0);
                var len = thickness;
                /** Polar X -> x = cos(angle) * length */
                /** Polar Y -> y = sin(angle) * length */

                /** First two points are calculated with the first angle, the same angle used for the previous segment */
                var p1 = {
                    x: x1 + Math.cos(angle1) * len,
                    y: y1 + Math.sin(angle1) * len
                };
                var p2 = {
                    x: x1 + Math.cos(angle1) * -len,
                    y: y1 + Math.sin(angle1) * -len
                };
                /** The other two points are calculated with the second angle */
                var p3 = {
                    x: x2 + Math.cos(angle2) * len,
                    y: y2 + Math.sin(angle2) * len
                };
                var p4 = {
                    x: x2 + Math.cos(angle2) * -len,
                    y: y2 + Math.sin(angle2) * -len
                };

                data[i++] = p1.x;
                data[i++] = p1.y;
                data[i++] = alpha;
                data[i++] = colorStart;

                data[i++] = p2.x;
                data[i++] = p2.y;
                data[i++] = alpha;
                data[i++] = colorStart;

                data[i++] = p4.x;
                data[i++] = p4.y;
                data[i++] = alpha;
                data[i++] = colorEnd;

                data[i++] = p4.x;
                data[i++] = p4.y;
                data[i++] = alpha;
                data[i++] = colorEnd;

                data[i++] = p3.x;
                data[i++] = p3.y;
                data[i++] = alpha;
                data[i++] = colorEnd;

                data[i++] = p1.x;
                data[i++] = p1.y;
                data[i++] = alpha;
                data[i++] = colorStart;
            }
            /** Returns the angle between two points in radians */
            function getAngle(x1, y1, x2, y2) {
                var deltaX = x2 - x1;
                var deltaY = y2 - y1;

                return Math.atan2(deltaY, deltaX);
            }
            /** Isolates the alpha value from the color string */
            function getAlpha(color) {
                if (!color) return 1;
                var c = color.split(",");

                var value = (c.length < 4) ? 1 : parseFloat(c[3].replace(")"));

                return value;
            }
            /** Divide color depending on the segment */
            function interpolateColor(colorStart, colorEnd, t) {
                var c = {}, c1 = {}, c2 = {};
                c1.b = colorStart % 256.0; colorStart = Math.floor(colorStart / 256.0);
                c1.g = colorStart % 256.0; colorStart = Math.floor(colorStart / 256.0);
                c1.r = colorStart % 256.0;

                c2.b = colorEnd % 256.0; colorEnd = Math.floor(colorEnd / 256.0);
                c2.g = colorEnd % 256.0; colorEnd = Math.floor(colorEnd / 256.0);
                c2.r = colorEnd % 256.0;

                c.b = c1.b + (c2.b - c1.b) * t;
                c.g = c1.g + (c2.g - c1.g) * t;
                c.r = c1.r + (c2.r - c1.r) * t;

                return (c.r << 16) + (c.g << 8) + (c.b);
            }

            var prevAngle = null;
            for (var j = 0; j < segments; j++) {
                /** t is the factor by which we calculate the distance of the point from the line connecting point 1 and point 2 */
                var t = j / segments;

                var p;
                if (source.id === target.id)
                    p = sigma.utils.getPointOnBezierCurve(t, x1, y1, x2, y2, cp.x1, cp.y1, cp.x2, cp.y2);
                else
                    p = sigma.utils.getPointOnQuadraticCurve(t, x1, y1, x2, y2, cp.x, cp.y);

                var xStart = p.x;
                var yStart = p.y;
                var c1 = interpolateColor(colorStart, colorEnd, t);
                /** t is recalculated for the next position */
                t = (j + 1) / segments;

                // p = sigma.utils.getPointOnQuadraticCurve(t, x1, y1, x2, y2, cp.x, cp.y);
                if (source.id === target.id)
                    p = sigma.utils.getPointOnBezierCurve(t, x1, y1, x2, y2, cp.x1, cp.y1, cp.x2, cp.y2);
                else
                    p = sigma.utils.getPointOnQuadraticCurve(t, x1, y1, x2, y2, cp.x, cp.y);

                var xEnd = p.x;
                var yEnd = p.y;
                var c2 = interpolateColor(colorStart, colorEnd, t);

                /** We need to calculate two angles for each segment (except for the first one), so that there won't be any gaps on our line */
                var currAngle = getAngle(xStart, yStart, xEnd, yEnd) + (Math.PI / 2.0);
                if (prevAngle == null)prevAngle = currAngle;
                addVerts(xStart, yStart, xEnd, yEnd, prevAngle, currAngle, data, c1, c2, alpha, w);
                prevAngle = currAngle;
            }
        },
        render: function (gl, program, data, params) {
            var buffer;

            // Define attributes:
            var colorLocation =
                    gl.getAttribLocation(program, 'a_color'),
                positionLocation =
                    gl.getAttribLocation(program, 'a_position'),
                alphaLocation =
                    gl.getAttribLocation(program, 'a_alpha'),
                resolutionLocation =
                    gl.getUniformLocation(program, 'u_resolution'),
                matrixLocation =
                    gl.getUniformLocation(program, 'u_matrix');

            buffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
            gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);

            gl.uniform2f(resolutionLocation, params.width, params.height);
            gl.uniformMatrix3fv(matrixLocation, false, params.matrix);

            gl.enableVertexAttribArray(positionLocation);
            gl.enableVertexAttribArray(alphaLocation);
            gl.enableVertexAttribArray(colorLocation);

            gl.vertexAttribPointer(positionLocation,
                2,
                gl.FLOAT,
                false,
                this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT,
                0
            );
            gl.vertexAttribPointer(alphaLocation,
                1,
                gl.FLOAT,
                false,
                this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT,
                8
            );
            gl.vertexAttribPointer(colorLocation,
                1,
                gl.FLOAT,
                false,
                this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT,
                12
            );

            //gl.lineWidth(3);
            gl.drawArrays(
                gl.TRIANGLES,
                params.start || 0,
                params.count || (data.length / this.ATTRIBUTES)
            );
        },
        initProgram: function (gl) {
            var vertexShader,
                fragmentShader,
                program;

            vertexShader = sigma.utils.loadShader(
                gl,
                [
                    'attribute vec2 a_position;',
                    'attribute float a_alpha;',
                    'attribute float a_color;',

                    'uniform vec2 u_resolution;',
                    'uniform mat3 u_matrix;',

                    'varying vec4 color;',

                    'void main() {',
                    // Scale from [[-1 1] [-1 1]] to the container:
                    'gl_Position = vec4(',
                    '((u_matrix * vec3(a_position, 1)).xy /',
                    'u_resolution * 2.0 - 1.0) * vec2(1, -1),',
                    '0,',
                    '1',
                    ');',

                    // Extract the color:
                    'float c = a_color;',
                    'color.b = mod(c, 256.0); c = floor(c / 256.0);',
                    'color.g = mod(c, 256.0); c = floor(c / 256.0);',
                    'color.r = mod(c, 256.0); c = floor(c / 256.0); color /= 255.0;',
                    'color.a = a_alpha;',
                    '}'
                ].join('\n'),
                gl.VERTEX_SHADER
            );

            fragmentShader = sigma.utils.loadShader(
                gl,
                [
                    'precision mediump float;',

                    'varying vec4 color;',

                    'void main(void) {',
                    'gl_FragColor = color;',
                    '}'
                ].join('\n'),
                gl.FRAGMENT_SHADER
            );

            program = sigma.utils.loadProgram(gl, [vertexShader, fragmentShader]);

            return program;
        }
    };
})();