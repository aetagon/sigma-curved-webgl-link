; (function () {
    'use strict';

    sigma.utils.pkg('sigma.webgl.nodes');

    /**
     * This node renderer will display nodes in the fastest way: Nodes are basic
     * squares, drawn through the gl.POINTS drawing method. The size of the nodes
     * are represented with the "gl_PointSize" value in the vertex shader.
     *
     * It is the fastest node renderer here since the buffer just takes one line
     * to draw each node (with attributes "x", "y", "size" and "color").
     *
     * Nevertheless, this method has some problems, especially due to some issues
     * with the gl.POINTS:
     *  - First, if the center of a node is outside the scene, the point will not
     *    be drawn, even if it should be partly on screen.
     *  - I tried applying a fragment shader similar to the one in the default
     *    node renderer to display them as discs, but it did not work fine on
     *    some computers settings, filling the discs with weird gradients not
     *    depending on the actual color.
     */
    sigma.webgl.nodes.squareBMP = {
        borderData: null,
        POINTS: 6,
        ATTRIBUTES: 5,
        addNode: function (node, data, i, prefix, settings) {
            var _this = this;

            var x = node[prefix + 'x'];
            var y = node[prefix + 'y'];
            var size = node[prefix + 'size'];
            var color = sigma.utils.floatColor(node.color || settings('defaultNodeColor'));
            var bColor = sigma.utils.floatColor(node.borderColor || settings('defaultNodeColor'));
            var bWidth = node.borderWidth || 1;

            if (i == 0) _this.borderData = new Float32Array(data.length);

            addSquare(x, y, size, color, bColor, 0, bWidth);

            function addSquare(x, y, size, color, borderColor, rotation, borderWidth) {
                var angle = (Math.PI / 4.0) + rotation;

                //Triangle points
                var newX = x + size * Math.cos(angle);
                var newY = y + size * Math.sin(angle);
                data[i++] = newX;
                data[i++] = newY;
                data[i++] = color;
                data[i++] = 1.0;
                data[i++] = 0.0;
                //Border points
                newX = x + (size + borderWidth) * Math.cos(angle);
                newY = y + (size + borderWidth) * Math.sin(angle);
                _this.borderData[i - 5] = newX;
                _this.borderData[i - 4] = newY;
                _this.borderData[i - 3] = borderColor;
                _this.borderData[i - 2] = 1.0;
                _this.borderData[i - 1] = 1.0;

                //Triangle points
                newX = x + size * Math.cos(angle - Math.PI / 2);
                newY = y + size * Math.sin(angle - Math.PI / 2);
                data[i++] = newX;
                data[i++] = newY;
                data[i++] = color;
                data[i++] = 1.0;
                data[i++] = 1.0;
                //Border points
                newX = x + (size + borderWidth) * Math.cos(angle - Math.PI / 2);
                newY = y + (size + borderWidth) * Math.sin(angle - Math.PI / 2);
                _this.borderData[i - 5] = newX;
                _this.borderData[i - 4] = newY;
                _this.borderData[i - 3] = borderColor;
                _this.borderData[i - 2] = 1.0;
                _this.borderData[i - 1] = 1.0;

                //Triangle points
                newX = x + size * Math.cos(angle - Math.PI);
                newY = y + size * Math.sin(angle - Math.PI);
                data[i++] = newX;
                data[i++] = newY;
                data[i++] = color;
                data[i++] = 0.0;
                data[i++] = 1.0;
                //Border points
                newX = x + (size + borderWidth) * Math.cos(angle - Math.PI);
                newY = y + (size + borderWidth) * Math.sin(angle - Math.PI);
                _this.borderData[i - 5] = newX;
                _this.borderData[i - 4] = newY;
                _this.borderData[i - 3] = borderColor;
                _this.borderData[i - 2] = 1.0;
                _this.borderData[i - 1] = 1.0;

                //Triangle points
                newX = x + size * Math.cos(angle - Math.PI);
                newY = y + size * Math.sin(angle - Math.PI);
                data[i++] = newX;
                data[i++] = newY;
                data[i++] = color;
                data[i++] = 0.0;
                data[i++] = 1.0;
                //Border points
                newX = x + (size + borderWidth) * Math.cos(angle - Math.PI);
                newY = y + (size + borderWidth) * Math.sin(angle - Math.PI);
                _this.borderData[i - 5] = newX;
                _this.borderData[i - 4] = newY;
                _this.borderData[i - 3] = borderColor;
                _this.borderData[i - 2] = 1.0;
                _this.borderData[i - 1] = 1.0;

                //Triangle points
                newX = x + size * Math.cos(angle + Math.PI / 2);
                newY = y + size * Math.sin(angle + Math.PI / 2);
                data[i++] = newX;
                data[i++] = newY;
                data[i++] = color;
                data[i++] = 0.0;
                data[i++] = 0.0;
                //Border points
                newX = x + (size + borderWidth) * Math.cos(angle + Math.PI / 2);
                newY = y + (size + borderWidth) * Math.sin(angle + Math.PI / 2);
                _this.borderData[i - 5] = newX;
                _this.borderData[i - 4] = newY;
                _this.borderData[i - 3] = borderColor;
                _this.borderData[i - 2] = 1.0;
                _this.borderData[i - 1] = 1.0;

                //Triangle points
                newX = x + size * Math.cos(angle);
                newY = y + size * Math.sin(angle);
                data[i++] = newX;
                data[i++] = newY;
                data[i++] = color;
                data[i++] = 1.0;
                data[i++] = 0.0;
                //Border points
                newX = x + (size + borderWidth) * Math.cos(angle);
                newY = y + (size + borderWidth) * Math.sin(angle);
                _this.borderData[i - 5] = newX;
                _this.borderData[i - 4] = newY;
                _this.borderData[i - 3] = borderColor;
                _this.borderData[i - 2] = 1.0;
                _this.borderData[i - 1] = 1.0;
            }
            
        },
        render: function (gl, program, data, params) {
            var _this = this;
            // Define attributes:
            var positionLocation =
                  gl.getAttribLocation(program, 'a_position'),
                colorLocation =
                  gl.getAttribLocation(program, 'a_color'),
                resolutionLocation =
                  gl.getUniformLocation(program, 'u_resolution'),
                matrixLocation =
                  gl.getUniformLocation(program, 'u_matrix'),
                ratioLocation =
                  gl.getUniformLocation(program, 'u_ratio'),
                scaleLocation =
                  gl.getUniformLocation(program, 'u_scale'),
                textureCoordLocation
                  = gl.getAttribLocation(program, "a_textureCoord");;

            gl.uniform2f(resolutionLocation, params.width, params.height);
            gl.uniform1f(
              ratioLocation,
              1 / Math.pow(params.ratio, params.settings('nodesPowRatio'))
            );
            gl.uniform1f(scaleLocation, params.scalingRatio);
            gl.uniformMatrix3fv(matrixLocation, false, params.matrix);

            gl.enableVertexAttribArray(positionLocation);
            gl.enableVertexAttribArray(colorLocation);
            gl.enableVertexAttribArray(textureCoordLocation);

            var texture = initTextures();


            //drawBuffer(this, this.borderData, texture);
            //drawBuffer(this, data, texture);


            function drawBuffer(node, bufferData, texture) {
                var buffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
                gl.bufferData(gl.ARRAY_BUFFER, bufferData, gl.DYNAMIC_DRAW);

                gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 0);
                gl.vertexAttribPointer(colorLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
                gl.vertexAttribPointer(textureCoordLocation, 2, gl.FLOAT, false, 0, 12);

                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, texture);
                gl.uniform1i(gl.getUniformLocation(program, "uSampler"), 0);

                /** By using the following code, we make sure that the shader won't take too much time each time, thus fail and cause restart of driver */
                var start = params.start || 0;
                var end = start + params.count || (bufferData.length / node.ATTRIBUTES);
                /** Each draw, will render 1000 nodes. This limit was set after testing */
                var step = node.POINTS * 1000;

                for (var i = start; i < end; i += step) {
                    var count = Math.min(step, end - i);
                    gl.drawArrays(
                        gl.TRIANGLES,
                        i,
                        count
                    );
                }
            }
            function initTextures() {
                var cubeTexture = gl.createTexture();
                var cubeImage = new Image();
                cubeImage.onload = function () {
                    handleTextureLoaded(cubeImage, cubeTexture);
                    drawBuffer(_this, data, cubeTexture);
                }
                cubeImage.src = "square_btmp.png";
            }
            function handleTextureLoaded(image, texture) {
                gl.bindTexture(gl.TEXTURE_2D, texture);
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
                
                gl.generateMipmap(gl.TEXTURE_2D);
                gl.bindTexture(gl.TEXTURE_2D, null);
            }
        },
        initProgram: function (gl) {
            var vertexShader,
                fragmentShader,
                program;

            vertexShader = sigma.utils.loadShader(
              gl,
              [
                'attribute vec2 a_position;',
                'attribute float a_color;',
                'attribute vec2 a_textureCoord;',

                'uniform vec2 u_resolution;',
                'uniform float u_ratio;',
                'uniform float u_scale;',
                'uniform mat3 u_matrix;',

                'varying vec4 color;',
                'varying highp vec2 vTextureCoord;',

                'void main() {',
                  // Scale from [[-1 1] [-1 1]] to the container:
                  'gl_Position = vec4(',
                    '((u_matrix * vec3(a_position, 1)).xy /',
                      'u_resolution * 2.0 - 1.0) * vec2(1, -1),',
                    '0,',
                    '1',
                  ');',

                  // Extract the color:
                  'float c = a_color;',
                  'color.b = mod(c, 256.0); c = floor(c / 256.0);',
                  'color.g = mod(c, 256.0); c = floor(c / 256.0);',
                  'color.r = mod(c, 256.0); c = floor(c / 256.0); color /= 255.0;',
                  'color.a = 1.0;',
                  'vTextureCoord = a_textureCoord;',
                '}'
              ].join('\n'),
              gl.VERTEX_SHADER
            );

            fragmentShader = sigma.utils.loadShader(
              gl,
              [
                'precision mediump float;',

                'varying vec4 color;',
                'varying highp vec2 vTextureCoord;',

                'uniform sampler2D uSampler;',

                'void main(void) {',
                  'float border = 0.01;',
                  'float radius = 0.5;',

                  'vec4 color0 = vec4(0.0, 0.0, 0.0, 0.0);',
                  'vec2 m = gl_PointCoord - vec2(0.5, 0.5);',
                  'float dist = radius - sqrt(m.x * m.x + m.y * m.y);',

                  'float t = 0.0;',
                  'if (dist > border)',
                    't = 1.0;',
                  'else if (dist > 0.0)',
                    't = dist / border;',

                  //'gl_FragColor = mix(color0, color, t);',

                  'gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));',
                '}'
              ].join('\n'),
              gl.FRAGMENT_SHADER
            );

            program = sigma.utils.loadProgram(gl, [vertexShader, fragmentShader]);

            return program;
        }
    };
})();
