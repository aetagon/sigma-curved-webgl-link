; (function () {
    'use strict';

    sigma.utils.pkg('sigma.webgl.nodes');

    /**
     * This node renderer will display nodes as squares using WebGL rendering
     */
    sigma.webgl.nodes.texture = {
        POINTS: 6,
        ATTRIBUTES: 9,
        icons: {
            canvas: null,
            count: 0,
            bitmaps: {},
            allLoaded: function () {
                for (var bmp in this.bitmaps) {
                    if (!this.bitmaps[bmp].loaded)
                        return false;
                }
                return true;
            },
            resetBmpUsed: function () {
                for (var bmp in this.bitmaps) {
                    this.bitmaps[bmp].used = false
                }
            },
            deleteUnsedBmps: function () {
                this.count = 0;
                for (var bmp in this.bitmaps) {
                    if (!this.bitmaps[bmp].used)
                        delete this.bitmaps[bmp];
                    else
                        this.count++;
                }
            },
            loadBmps: function () {
                var _this = sigma.webgl.nodes.texture;

                initCanvas();
                for (var bmp in this.bitmaps) {
                    if (this.bitmaps[bmp].url)
                        this.loadUrl(this.bitmaps[bmp].url)
                    else if (this.bitmaps[bmp].icon)
                        this.loadText(this.bitmaps[bmp].icon)
                };
                
                function initCanvas() {
                    _this.icons.canvas = document.createElement("canvas");
                    _this.icons.canvas.width = _this.props.icons.width;
                    _this.icons.canvas.height = _this.props.icons.height;
                    //_this.icons.count = 0;
                    //_this.icons.bitmaps = {};
                }
            },
            loadUrl: function (bmpUrl) {
                var img = new Image();
                var iconOffsetX = this.bitmaps[bmpUrl].offsetX;
                var iconOffsetY = this.bitmaps[bmpUrl].offsetY;
                var _this = sigma.webgl.nodes.texture;
                img.onload = function () {
                    var ctx = _this.icons.canvas.getContext("2d");
                    ctx.drawImage(
                      img, iconOffsetX, iconOffsetY,
                      _this.props.icons.iconsize, _this.props.icons.iconsize
                    );
                    _this.icons.bitmaps[bmpUrl].loaded = true;
                }
                img.setAttribute('crossOrigin', 'Anonymous');
                img.src = bmpUrl;
            },
            loadText: function (icon) {
                var _this = sigma.webgl.nodes.texture;
                var ctx = this.canvas.getContext("2d");
                var fgColor = icon.color;
                var fontSize = _this.props.icons.iconsize;
                var font = icon.font;
                var text = icon.content;
                var iconOffsetX = this.bitmaps[icon.content].offsetX;
                var iconOffsetY = this.bitmaps[icon.content].offsetY;

                var x = iconOffsetX + fontSize / 2;
                var y = iconOffsetY + fontSize / 2;

                ctx.beginPath();
                ctx.fillStyle = fgColor;
                ctx.font = '' + (fontSize * 0.7) + 'px ' + font;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(text, x, y);

                this.bitmaps[icon.content].loaded = true;
            }
        },
        props: {
            icons: { width: 2048, height: 2048, iconsize: 32 }
        },
        addNode: function (node, data, i, prefix, settings) {
            var _this = this;
            if (i == 0) {
                //Reset icons object
                //_this.icons.canvas = null;
                this.icons.resetBmpUsed();
            }

            var x = node[prefix + 'x'];
            var y = node[prefix + 'y'];
            var size = node[prefix + 'size'];
            var color = sigma.utils.floatColor(node.color || settings('defaultNodeColor'));
            var bColor = sigma.utils.floatColor(node.borderColor || settings('defaultNodeColor'));
            var bWidth = node.borderWidth || 0.0;
            var clip = (node.image) ? node.image.clip : 1;
            var iconOffsetX = 0;
            var iconOffsetY = 0;
            var squareToCircleRatio = (node.image || node.icon) ? (node.image || node.icon).scale : 1;
            var dimensions = {
                w: (node.image) ? node.image.w || 1 : 1,
                h: (node.image) ? node.image.h || 1 : 1
            };
            fixDimensions(dimensions);

            loadIconImage(node);

            addSquare(x, y, size, clip, dimensions); //6 Points

            function addSquare(x, y, size, clip, dimensions) {
                var angle = (Math.PI / 4.0);
                size = size * squareToCircleRatio;

                //Triangle points
                data[i++] = x;
                data[i++] = y;
                data[i++] = clip;
                data[i++] = size;
                data[i++] = angle;
                data[i++] = getAbsoluteUV(1, iconOffsetX, _this.props.icons.width);
                data[i++] = getAbsoluteUV(1, iconOffsetY, _this.props.icons.height);
                data[i++] = dimensions.w;
                data[i++] = dimensions.h;
                //Triangle points
                data[i++] = x;
                data[i++] = y;
                data[i++] = clip;
                data[i++] = size;
                data[i++] = angle - Math.PI / 2;
                data[i++] = getAbsoluteUV(1, iconOffsetX, _this.props.icons.width);
                data[i++] = getAbsoluteUV(0, iconOffsetY, _this.props.icons.height);
                data[i++] = dimensions.w;
                data[i++] = dimensions.h;
                //Triangle points
                data[i++] = x;
                data[i++] = y;
                data[i++] = clip;
                data[i++] = size;
                data[i++] = angle - Math.PI;
                data[i++] = getAbsoluteUV(0, iconOffsetX, _this.props.icons.width);
                data[i++] = getAbsoluteUV(0, iconOffsetY, _this.props.icons.height);
                data[i++] = dimensions.w;
                data[i++] = dimensions.h;

                //Triangle points
                data[i++] = x;
                data[i++] = y;
                data[i++] = clip;
                data[i++] = size;
                data[i++] = angle - Math.PI;
                data[i++] = getAbsoluteUV(0, iconOffsetX, _this.props.icons.width);
                data[i++] = getAbsoluteUV(0, iconOffsetY, _this.props.icons.height);
                data[i++] = dimensions.w;
                data[i++] = dimensions.h;
                //Triangle points
                data[i++] = x;
                data[i++] = y;
                data[i++] = clip;
                data[i++] = size;
                data[i++] = angle + Math.PI / 2;
                data[i++] = getAbsoluteUV(0, iconOffsetX, _this.props.icons.width);
                data[i++] = getAbsoluteUV(1, iconOffsetY, _this.props.icons.height);
                data[i++] = dimensions.w;
                data[i++] = dimensions.h;
                //Triangle points
                data[i++] = x;
                data[i++] = y;
                data[i++] = clip;
                data[i++] = size;
                data[i++] = angle;
                data[i++] = getAbsoluteUV(1, iconOffsetX, _this.props.icons.width);
                data[i++] = getAbsoluteUV(1, iconOffsetY, _this.props.icons.height);
                data[i++] = dimensions.w;
                data[i++] = dimensions.h;
            }
            function getAbsoluteUV(relUV, offsetPix, totalWidth) {
                var ratio = _this.props.icons.iconsize / totalWidth;
                var offset = offsetPix / totalWidth;

                return Math.min(1, relUV) * ratio + offset;
            }

            function loadIconImage(node) {
                if (node.image) {
                    var url = node.image.url;
                    setOffsets(url);
                    _this.icons.bitmaps[url].url = url;
                }
                    //    loadImage(node.image);
                else if (node.icon) {
                    var content = node.icon.content;
                    setOffsets(content);
                    _this.icons.bitmaps[content].icon = node.icon;
                }
                //    loadText(node.icon);
            }
            function setOffsets(imageSource) {
                if (_this.icons.bitmaps[imageSource]) {
                    iconOffsetX = _this.icons.bitmaps[imageSource].offsetX;
                    iconOffsetY = _this.icons.bitmaps[imageSource].offsetY;
                    _this.icons.bitmaps[imageSource].used = true;
                    return true;
                }
                else {
                    iconOffsetX = getOffsetX();
                    iconOffsetY = getOffsetY();
                    _this.icons.count++;
                    _this.icons.bitmaps[imageSource] = {
                        offsetX: iconOffsetX,
                        offsetY: iconOffsetY
                    }
                    _this.icons.bitmaps[imageSource].used = true;
                    _this.icons.hasNew = true;
                    return false;
                }
            }
            //function loadImage(image) {
            //    var url = image.url;
            //    if (!setOffsets(url)) {
            //        var img = new Image();
            //        img.setAttribute('crossOrigin', 'Anonymous');
            //        img.onload = function () {
            //            var ctx = _this.icons.canvas.getContext("2d");
            //            ctx.drawImage(
            //              img, iconOffsetX, iconOffsetY,
            //              _this.props.icons.iconsize, _this.props.icons.iconsize
            //            );
            //            _this.icons.bitmaps[url].loaded = true;
            //        }
            //        img.src = url;
            //    }
            //}
            //function loadText(icon) {
            //    if (!setOffsets(icon.content)) {
            //        var ctx = _this.icons.canvas.getContext("2d");
            //        var fgColor = icon.color;
            //        var fontSize = _this.props.icons.iconsize;
            //        var font = icon.font;
            //        var text = icon.content;
            //        var x = iconOffsetX + fontSize / 2;
            //        var y = iconOffsetY + fontSize / 2;
            //        ctx.beginPath();
            //        ctx.fillStyle = fgColor;
            //        ctx.font = '' + (fontSize * 0.7) + 'px ' + font;
            //        ctx.textAlign = 'center';
            //        ctx.textBaseline = 'middle';
            //        ctx.fillText(text, x, y);
            //        _this.icons.bitmaps[icon.content].loaded = true;
            //    }
            //}
            function initCanvas() {
                if (_this.icons.canvas === null) {
                    _this.icons.canvas = document.createElement("canvas");
                    _this.icons.canvas.width = _this.props.icons.width;
                    _this.icons.canvas.height = _this.props.icons.height;
                    _this.icons.count = 0;
                    _this.icons.bitmaps = {};
                }
                //_this.icons.texture = null;
            }
            function getOffsetX() {
                var count = _this.icons.count;
                var width = _this.props.icons.iconsize;
                var totalWidth = _this.props.icons.width;
                return (count * width) % totalWidth;
            }
            function getOffsetY() {
                var count = _this.icons.count;
                var width = _this.props.icons.iconsize;
                var totalWidth = _this.props.icons.width;
                return Math.floor((count * width) / totalWidth) * width;
            }
            function fixDimensions(dimensions) {
                if (dimensions.w > 1) {
                    dimensions.h /= dimensions.w;
                    dimensions.w /= dimensions.w;
                };
                if (dimensions.h > 1) {
                    dimensions.w /= dimensions.h;
                    dimensions.h /= dimensions.h;
                };
            }

        },
        render: function (gl, program, data, params) {
            var _this = this;
            var args = arguments;

            _this.icons.deleteUnsedBmps();
            _this.icons.loadBmps();
            innerRender(gl, program, data, params);

            function innerRender(gl, program, data, params) {
                if (!initTextures())
                    return;
                // Define attributes:
                var positionLocation = gl.getAttribLocation(program, 'a_position'),
                    clipLocation = gl.getAttribLocation(program, 'a_clip'),
                    sizeLocation = gl.getAttribLocation(program, 'a_size'),
                    angleLocation = gl.getAttribLocation(program, 'a_angle'),
                    textcoordLocation = gl.getAttribLocation(program, 'a_texturecoords'),
                    dimensionsLocation = gl.getAttribLocation(program, 'a_dimensions'),
                    resolutionLocation = gl.getUniformLocation(program, 'u_resolution'),
                    matrixLocation = gl.getUniformLocation(program, 'u_matrix'),
                    ratioLocation = gl.getUniformLocation(program, 'u_ratio'),
                    scaleLocation = gl.getUniformLocation(program, 'u_scale');

                gl.uniform2f(resolutionLocation, params.width, params.height);
                gl.uniform1f(ratioLocation, Math.pow(params.ratio, params.settings('nodesPowRatio')));
                gl.uniform1f(scaleLocation, params.scalingRatio);
                gl.uniformMatrix3fv(matrixLocation, false, params.matrix);

                gl.enableVertexAttribArray(positionLocation);
                gl.enableVertexAttribArray(clipLocation);
                gl.enableVertexAttribArray(sizeLocation);
                gl.enableVertexAttribArray(angleLocation);
                gl.enableVertexAttribArray(textcoordLocation);
                gl.enableVertexAttribArray(dimensionsLocation);

                var buffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
                gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
                drawBuffer(_this, buffer, data);

                function drawBuffer(node, buffer, bufferData) {
                    /** By using the following code, we make sure that the shader won't take too much time each time, thus fail and cause restart of driver */
                    var start = params.start || 0;
                    var end = start + params.count || (bufferData.length / node.ATTRIBUTES);
                    /** Each draw, will render 100 nodes. This limit was set after testing */
                    var step = node.POINTS * 100;

                    // Specify the texture to map onto the faces.
                    gl.activeTexture(gl.TEXTURE0);
                    gl.bindTexture(gl.TEXTURE_2D, _this.icons.texture);
                    gl.uniform1i(gl.getUniformLocation(program, "uSampler"), 0);

                    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
                    gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 0);
                    gl.vertexAttribPointer(clipLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
                    gl.vertexAttribPointer(sizeLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 12);
                    gl.vertexAttribPointer(angleLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 16);
                    gl.vertexAttribPointer(textcoordLocation, 2, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 20);
                    gl.vertexAttribPointer(dimensionsLocation, 2, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 28);

                    for (var i = start; i < end; i += step) {
                        var count = Math.min(step, end - i);
                        gl.drawArrays(gl.TRIANGLES, i, count);
                    }
                }
                function initTextures() {
                    if (!_this.icons.allLoaded()) {
                        setTimeout(function () {
                            gl.useProgram(program);
                            innerRender.apply(_this, args);
                        }, 100);
                        return false;
                    }

                    if (!gl.isTexture(_this.icons.texture) || _this.icons.hasNew) {
                        _this.icons.hasNew = false;
                        _this.icons.texture = gl.createTexture();
                        handleTextureLoaded(_this.icons.canvas, _this.icons.texture)
                    }
                    return true;
                }
                function handleTextureLoaded(canvas, texture) {
                    gl.bindTexture(gl.TEXTURE_2D, texture);
                    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, canvas);
                    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
                    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
                    gl.generateMipmap(gl.TEXTURE_2D);
                    gl.bindTexture(gl.TEXTURE_2D, null);
                }
            }
        },
        initProgram: function (gl) {
            var vertexShader,
                fragmentShader,
                program;

            vertexShader = sigma.utils.loadShader(
              gl,
              [
                'attribute vec2 a_position;',
                'attribute float a_clip;',
                'attribute float a_size;',
                'attribute float a_angle;',
                'attribute vec2 a_texturecoords;',
                'attribute vec2 a_dimensions;',

                'uniform vec2 u_resolution;',
                'uniform float u_ratio;',
                'uniform float u_scale;',
                'uniform mat3 u_matrix;',

                'varying highp vec2 v_texturecoords;',
                'varying vec2 center;',
                'varying float radius;',

                'vec2 transPoint(vec2 p, vec2 dim) {',
                    'float x, y;',
                    'float size = a_size * u_ratio;',
                    'x = p.x + (dim.x * size * cos(a_angle));',
                    'y = p.y + (dim.y * size * sin(a_angle));',
                    'vec2 newP = vec2(x, y);',
                    'return newP;',
                '}',

                'void main() {',
                  'vec2 pos =  transPoint(a_position, a_dimensions);',
                  // Scale from [[-1 1] [-1 1]] to the container:                  
                  'vec4 pos1 = vec4(',
                    '((u_matrix * vec3(pos, 1)).xy /',
                        'u_resolution * 2.0 - 1.0) * vec2(1, -1),',
                    '0,',
                    '1',
                    ');',
                  'gl_Position = pos1;',

                  'vec2 position = (u_matrix * vec3(a_position, 1)).xy;',
                  'center = position * u_scale;',
                  'center = vec2(center.x, u_scale * u_resolution.y - center.y);',

                  'radius = (a_size * a_clip * 2.0) / u_ratio;',

                  'v_texturecoords = a_texturecoords;',
                '}'
              ].join('\n'),
              gl.VERTEX_SHADER
            );

            fragmentShader = sigma.utils.loadShader(
              gl,
              [
                'precision mediump float;',

                'varying highp vec2 v_texturecoords;',
                'varying vec2 center;',
                'varying float radius;',

                'uniform sampler2D uSampler;',

                'void main(void) {',
                  'vec2 m = gl_FragCoord.xy - center;',
                  'float diff = radius - sqrt(m.x * m.x + m.y * m.y);',

                  'vec4 tex_color = texture2D(uSampler, vec2(v_texturecoords.s, v_texturecoords.t));',
                  'if (diff < 0.0)',
                    'tex_color.a = 0.0;',
                  'gl_FragColor = tex_color;',
                '}'
              ].join('\n'),
              gl.FRAGMENT_SHADER
            );

            program = sigma.utils.loadProgram(gl, [vertexShader, fragmentShader]);

            return program;
        }
    };
})();
