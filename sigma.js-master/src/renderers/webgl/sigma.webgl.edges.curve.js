;(function () {
    'use strict';

    sigma.utils.pkg('sigma.webgl.edges');

    /**
     * This edge renderer will display edges as curved lines with the gl.TRIANGLES display
     * mode. With this, the user can set any width size. The line will be drawn in blocks 
     * in order to simulate the curve of the edge.
     * The number of blocks is defined by the p_segments variable.
     */

    /** Global variables */
    var g_buffer, g_program;
    var p_segments = 10, atLocs = {};
    sigma.webgl.edges.curve = {
        POINTS: 6 * p_segments, /** #sk: segments are the divided points for the Bezier line. This will become dynamic */
        ATTRIBUTES: 4,
        addEdge: function (edge, source, target, data, i, prefix, settings, customSegments) {
            var segments = customSegments || p_segments;

            var w = (edge['size'] || 1) / 2,
                x1 = source[prefix + 'x'],
                y1 = source[prefix + 'y'],
                x2 = target[prefix + 'x'],
                y2 = target[prefix + 'y'],
                color = edge.color,
                colorStart, colorEnd,
                count = edge.count || 0,
                alpha = getAlpha(color);

            if (!color) {
				switch (settings("edgeColor")) {
					case 'mixed':
						colorStart = sigma.utils.floatColor(source.color);
						colorEnd = sigma.utils.floatColor(target.color);
						break;
					case 'source':
						color = source.color || defaultEdgeColor;
						color = sigma.utils.floatColor(color);
						colorStart = colorEnd = color;
						break;
					case 'target':
						color = target.color || defaultEdgeColor;
						color = sigma.utils.floatColor(color);
						colorStart = colorEnd = color;
						break;
					default:
						color = defaultEdgeColor;
						color = sigma.utils.floatColor(color);
						colorStart = colorEnd = color;
						break;
				}
			} else {
				color = sigma.utils.floatColor(color);
				colorStart = colorEnd = color;	
			}

            /** Speed improvement */
            // if (colorStart && colorEnd) {
            //     // Normalize star/end color:
            //     colorStart = sigma.utils.floatColor(colorStart);
            //     colorEnd = sigma.utils.floatColor(colorEnd);
            // }
            // else {
            //     // Normalize color:
            //     color = sigma.utils.floatColor(color);
            //     colorStart = colorEnd = color;
            // }

            /** Setting up control point */
            var cp;
            var sameNode = source.id === target.id;
            if (sameNode) {
                cp = sigma.utils.getSelfLoopControlPoints(x1, y1, source[prefix + 'size']);
            }
            else {
                cp = sigma.utils.getQuadraticControlPoint(x1, y1, x2, y2, count);
            }

            function addVerts(x1, y1, x2, y2, angle1, angle2, data, colorStart, colorEnd, alpha, thickness, t1, t2) {
                var len = thickness;

                data[i++] = x1 + len * Math.cos(angle1);
                data[i++] = y1 + len * Math.sin(angle1);
                //data[i++] = angle1;
                data[i++] = alpha;
                data[i++] = colorStart;
                //data[i++] = colorEnd;
                //data[i++] = len;
                //data[i++] = t1;

                data[i++] = x1 + (-len) * Math.cos(angle1);
                data[i++] = y1 + (-len) * Math.sin(angle1);
                //data[i++] = angle1;
                data[i++] = alpha;
                data[i++] = colorStart;
                //data[i++] = colorEnd;
                //data[i++] = -len;
                //data[i++] = t1;

                data[i++] = x2 + (-len) * Math.cos(angle2);
                data[i++] = y2 + (-len) * Math.sin(angle2);
                //data[i++] = angle2;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = -len;
                //data[i++] = t2;

                data[i++] = x2 + (-len) * Math.cos(angle2);
                data[i++] = y2 + (-len) * Math.sin(angle2);
                //data[i++] = angle2;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = -len;
                //data[i++] = t2;

                data[i++] = x2 + len * Math.cos(angle2);
                data[i++] = y2 + len * Math.sin(angle2);
                //data[i++] = angle2;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = len;
                //data[i++] = t2;

                data[i++] = x1 + len * Math.cos(angle1);
                data[i++] = y1 + len * Math.sin(angle1);
                //data[i++] = angle1;
                data[i++] = alpha;
                data[i++] = colorStart;
                //data[i++] = colorEnd;
                //data[i++] = len;
                //data[i++] = t1;
            }

            /** Returns the angle between two points in radians */
            function getAngle(x1, y1, x2, y2) {
                var deltaX = x2 - x1;
                var deltaY = y2 - y1;

                var res = Math.atan2(deltaY, deltaX);

                // /** New faster getAngle implementation */
                // var Xa = 1;
                // var Ya = 0;
                //
                // // var Xb = Math.abs(deltaX);
                // // var Yb = Math.abs(deltaY);
                // var Xb = deltaX;
                // var Yb = deltaY;
                //
                // var dotProd = Xa * Xb + Ya * Yb;
                // var length1 = Math.sqrt(Xa * Xa + Ya * Ya);
                // var length2 = Math.sqrt(Xb * Xb + Yb * Yb);
                //
                // var res = Math.acos(dotProd / (length1 * length2));// * (Xb / deltaX * Yb / deltaY);
                //
                // console.log(res + " " + (res * (180 / Math.PI)) + " - " + Math.cos(Math.abs(res)) * (res/Math.abs(res)));
                return res;
            }

            /** Isolates the alpha value from the color string */
            function getAlpha(color) {
                if (!color) return 1;
                var c = color.split(",");

                var value = (c.length < 4) ? 1 : parseFloat(c[3].replace(")"));

                return value;
            }

            function interpolateColor(colorStart, colorEnd, _t) {
                var c = {}, c1 = {}, c2 = {};
                c1.b = colorStart % 256.0; colorStart = Math.floor(colorStart / 256.0);
                c1.g = colorStart % 256.0; colorStart = Math.floor(colorStart / 256.0);
                c1.r = colorStart % 256.0;

                c2.b = colorEnd % 256.0; colorEnd = Math.floor(colorEnd / 256.0);
                c2.g = colorEnd % 256.0; colorEnd = Math.floor(colorEnd / 256.0);
                c2.r = colorEnd % 256.0;

                c.b = (c1.b + (c2.b - c1.b) * _t);
                c.g = (c1.g + (c2.g - c1.g) * _t);
                c.r = (c1.r + (c2.r - c1.r) * _t);

                var color = c.r << 16;
                color += c.g << 8;
                color += c.b;

                return Math.round(color);
            }

            var prevAngle = null;
            var t1, t2, p, xStart, yStart, xEnd, yEnd;
            for (var j = 0; j < segments; j++) {
                /** t is the factor by which we calculate the distance of the point from the line connecting point 1 and point 2 */
                t1 = j / segments;
                /** t is recalculated for the next position */
                t2 = (j + 1) / segments;

                if (!sameNode) {
                    p = sigma.utils.getPointOnQuadraticCurve(t1, x1, y1, x2, y2, cp.x, cp.y);
                    xStart = p.x;
                    yStart = p.y;

                    p = sigma.utils.getPointOnQuadraticCurve(t2, x1, y1, x2, y2, cp.x, cp.y);
                    xEnd = p.x;
                    yEnd = p.y;
                }
                else {
                    p = sigma.utils.getPointOnBezierCurve(t1, x1, y1, x2, y2, cp.x1, cp.y1, cp.x2, cp.y2);
                    xStart = p.x;
                    yStart = p.y;

                    p = sigma.utils.getPointOnBezierCurve(t2, x1, y1, x2, y2, cp.x1, cp.y1, cp.x2, cp.y2);
                    xEnd = p.x;
                    yEnd = p.y;
                }
                var color1 = interpolateColor(colorStart, colorEnd, t1);
                var color2 = interpolateColor(colorStart, colorEnd, t2);
                /** We need to calculate two angles for each segment (except for the first one), so that there won't be any gaps on our line */
                var currAngle = getAngle(xStart, yStart, xEnd, yEnd) + (Math.PI / 2.0);
                if (prevAngle == null)prevAngle = currAngle;
                addVerts(xStart, yStart, xEnd, yEnd, prevAngle, currAngle, data, color1, color2, alpha, w, t1, t2);
                prevAngle = currAngle;
            }
        },
        render: function (gl, program, data, params) {
            var buffer = g_buffer;
            var update = (!atLocs.colorLocation1);

            // Define attributes:
            if (update) {
                atLocs.colorLocation1 = gl.getAttribLocation(program, 'a_color1');
                //atLocs.colorLocation2 = gl.getAttribLocation(program, 'a_color2');
                atLocs.positionLocation = gl.getAttribLocation(program, 'a_position');
                //atLocs.angleLocation = gl.getAttribLocation(program, 'a_angle');
                atLocs.alphaLocation = gl.getAttribLocation(program, 'a_alpha');
                //atLocs.dirLocation = gl.getAttribLocation(program, 'a_dir');
                //atLocs.tauLocation = gl.getAttribLocation(program, 'a_tau');

                atLocs.resolutionLocation = gl.getUniformLocation(program, 'u_resolution');
                atLocs.matrixLocation = gl.getUniformLocation(program, 'u_matrix');
            }

            if (update) {
                buffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
                
                g_buffer = buffer;
            }
            gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);

            gl.uniform2f(atLocs.resolutionLocation, params.width, params.height);
            gl.uniformMatrix3fv(atLocs.matrixLocation, false, params.matrix);

            if (update) {
                gl.enableVertexAttribArray(atLocs.positionLocation);
                gl.enableVertexAttribArray(atLocs.alphaLocation);
                gl.enableVertexAttribArray(atLocs.colorLocation1);
                //gl.enableVertexAttribArray(atLocs.colorLocation2);
                //gl.enableVertexAttribArray(atLocs.dirLocation);
                //gl.enableVertexAttribArray(atLocs.angleLocation);
                //gl.enableVertexAttribArray(atLocs.tauLocation);
            }
            gl.vertexAttribPointer(atLocs.positionLocation, 2, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 0);
            //gl.vertexAttribPointer(atLocs.angleLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
            gl.vertexAttribPointer(atLocs.alphaLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
            gl.vertexAttribPointer(atLocs.colorLocation1, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 12);
            //gl.vertexAttribPointer(atLocs.colorLocation2, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 20);
            //gl.vertexAttribPointer(atLocs.dirLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 24);
            //gl.vertexAttribPointer(atLocs.tauLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 28);

            /** By using the following code, we make sure that the shader won't take too much time each time, thus fail and cause restart of driver */
            var start = params.start || 0;
            var end = start + params.count || (data.length / this.ATTRIBUTES);
            /** Each draw, will render 1000 points. This limit was set after testing */
            var step = this.POINTS * 1000;

            for (var i = start; i < end; i += step) {
                var count = Math.min(step, end - i);
                gl.drawArrays(
                    gl.TRIANGLES,
                    i,
                    count
                );
            }
        },
        initProgram: function (gl) {
            var vertexShader,
                fragmentShader,
                program = g_program;
            atLocs = {};
            if (true) {
                vertexShader = sigma.utils.loadShader(
                    gl,
                    [
                        'attribute vec2 a_position;',
                        //'attribute float a_angle;',
                        'attribute float a_alpha;',
                        'attribute float a_color1;',
                        //'attribute float a_color2;',
                        //'attribute float a_dir;',
                        //'attribute float a_tau;',

                        'uniform vec2 u_resolution;',
                        'uniform mat3 u_matrix;',

                        'varying vec4 color;',

                        // 'float getAngle(vec2 pos1, vec2 pos2) {',
                        // // '    float deltaX = pos2.x - pos1.x;',
                        // // '    float deltaY = pos2.y - pos1.x;',
                        // //
                        // // '    return atan(deltaY, deltaX);',
                        // '      return acos(dot(normalize(pos1), normalize(pos2)));',
                        // '}',

                        'vec3 interpolateColor(float colorStart, float colorEnd, float _t) {',
                        '	vec3 c, c1, c2;',
                        '	c1.b = mod(colorStart, 256.0); colorStart = floor(colorStart / 256.0);',
                        '	c1.g = mod(colorStart, 256.0); colorStart = floor(colorStart / 256.0);',
                        '	c1.r = mod(colorStart, 256.0);',

                        '	c2.b = mod(colorEnd, 256.0); colorEnd = floor(colorEnd / 256.0);',
                        '	c2.g = mod(colorEnd, 256.0); colorEnd = floor(colorEnd / 256.0);',
                        '	c2.r = mod(colorEnd, 256.0);',

                        '	c.b = (c1.b + (c2.b - c1.b) * _t) / 255.0;',
                        '	c.g = (c1.g + (c2.g - c1.g) * _t) / 255.0;',
                        '	c.r = (c1.r + (c2.r - c1.r) * _t) / 255.0;',

                        '	return c;',
                        '}',

                        'void main() {',
                        // Scale from [[-1 1] [-1 1]] to the container:
                        //'   float angle = a_angle;',
                        '   float x = a_position.x;',
                        '   float y = a_position.y;',
                        '   vec2 finalPos = vec2(x, y);',
                        '   gl_Position = vec4(',
                        '      ((u_matrix * vec3(finalPos, 1)).xy /',
                        '      u_resolution * 2.0 - 1.0) * vec2(1, -1),',
                        '      0,',
                        '      1',
                        '   );',

                        // Extract the color:
                         'float c = a_color1;',
                         'color.b = mod(c, 256.0); c = floor(c / 256.0);',
                         'color.g = mod(c, 256.0); c = floor(c / 256.0);',
                         'color.r = mod(c, 256.0); c = floor(c / 256.0); color /= 255.0;',
                         'color.a = a_alpha;',
                        //'   color = vec4(interpolateColor(a_color1, a_color2, a_tau), a_alpha);',
                        '}'
                    ].join('\n'),
                    gl.VERTEX_SHADER
                );

                fragmentShader = sigma.utils.loadShader(
                    gl,
                    [
                        'precision mediump float;',

                        'varying vec4 color;',

                        'void main(void) {',
                        '   gl_FragColor = color;',
                        '}'
                    ].join('\n'),
                    gl.FRAGMENT_SHADER
                );

                program = sigma.utils.loadProgram(gl, [vertexShader, fragmentShader]);

                g_program = program;
            }

            return program;
        }
    };
})();