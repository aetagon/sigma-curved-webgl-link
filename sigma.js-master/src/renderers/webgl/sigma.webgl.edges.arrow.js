; (function () {
    'use strict';

    sigma.utils.pkg('sigma.webgl.edges');

    /**
     * This edge renderer will display edges as straight lines with arrow tips using the gl.TRIANGLES display
     * mode. With this, the user can set any width size. The line will be drawn in blocks.The arrow head is drawn
     * as a signle triangle with each side defined as the width size multiplied by the "arrowRatio" setting. Default value
     * is 7.5.
     */

    /** Global variables */
    var g_buffer, g_program;
    var atLocs = {};
    sigma.webgl.edges.arrow = {
        POINTS: 9,
        ATTRIBUTES: 6,
        addEdge: function (edge, source, target, data, i, prefix, settings) {

            var w = (edge['size'] || 1) / 2,
                x1 = source[prefix + 'x'],
                y1 = source[prefix + 'y'],
                x2 = target[prefix + 'x'],
                y2 = target[prefix + 'y'],
                color = edge.color,
                colorStart, colorEnd,
                count = edge.count || 0,
                alpha = getAlpha(color),
                arrowRatio = settings("arrowSizeRatio") || 7.5,
                nodeSize = target[prefix + 'size'] + (target.borderWidth || 0) * 0.08;

            arrowRatio = Math.max(settings("minArrowSize") / (w * 2), arrowRatio) || arrowRatio;
            arrowRatio = Math.min(settings("maxArrowSize") / (w * 2), arrowRatio) || arrowRatio;

            if (!color) {
                switch (settings("edgeColor")) {
                    case 'mixed':
                        colorStart = sigma.utils.floatColor(source.color);
                        colorEnd = sigma.utils.floatColor(target.color);
                        break;
                    case 'source':
                        color = source.color || defaultEdgeColor;
                        color = sigma.utils.floatColor(color);
                        colorStart = colorEnd = color;
                        break;
                    case 'target':
                        color = target.color || defaultEdgeColor;
                        color = sigma.utils.floatColor(color);
                        colorStart = colorEnd = color;
                        break;
                    default:
                        color = defaultEdgeColor;
                        color = sigma.utils.floatColor(color);
                        colorStart = colorEnd = color;
                        break;
                }
            } else {
                color = sigma.utils.floatColor(color);
                colorStart = colorEnd = color;
            }

            /** Setting up control point */
            var cp;
            var sameNode = source.id === target.id;
            if (sameNode) {
                cp = sigma.utils.getSelfLoopControlPoints(x1, y1, source[prefix + 'size']);
            }
            else {
                cp = sigma.utils.getQuadraticControlPoint(x1, y1, x2, y2, count);
            }

            function addVerts(x1, y1, x2, y2, angle, data, colorStart, colorEnd, alpha, thickness, t1, t2, nodeSize, arrowRatio) {
                var len = thickness;

                /* The following code block calculates the displacement of the body rectangle, relative to the size of the arrow head.
                 * In more detail, the two points (on the target node) are moved to the base of the arrow tip triangle.
                 * Triangle calculations are used.
                 */
                var a = (2 * arrowRatio * len) * Math.cos(Math.PI / 6);
                var b = len;
                /* "c" is the distance and "thita" is the angle, that x and y will be moved towards. This will place one side of the last rectangle right next to the arrow head */
                var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
                var thita = Math.acos(a / c);

                
                data[i++] = x1 + len * Math.cos(angle + Math.PI / 2);
                data[i++] = y1 + len * Math.sin(angle + Math.PI / 2);
                //data[i++] = angle + Math.PI / 2;
                data[i++] = alpha;
                data[i++] = colorStart;
                //data[i++] = colorEnd;
                //data[i++] = len;
                //data[i++] = t1;
                data[i++] = 0;
                data[i++] = angle

                data[i++] = x1 + (-len) * Math.cos(angle + Math.PI / 2);
                data[i++] = y1 + (-len) * Math.sin(angle + Math.PI / 2);
                //data[i++] = angle + Math.PI / 2;
                data[i++] = alpha;
                data[i++] = colorStart;
                //data[i++] = colorEnd;
                //data[i++] = -len;
                //data[i++] = t1;
                data[i++] = 0;
                data[i++] = angle

                data[i++] = x2 + c * Math.cos(angle - thita);
                data[i++] = y2 + c * Math.sin(angle - thita);
                //data[i++] = angle - thita;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = c;
                //data[i++] = t2;
                /* Arrows need to be placed next to the node, not in the center of it. The parameter "nodeSize" defines how much will the endpoint/startpoint will be moved due to the nature of the arrow edge. */
                data[i++] = nodeSize;
                /* We also pass to the shader the angle of of this edge's star, end points, in order to be able to use it will "offsetStart". */
                data[i++] = angle

                data[i++] = x2 + c * Math.cos(angle - thita);
                data[i++] = y2 + c * Math.sin(angle - thita);
                //data[i++] = angle - thita;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = c;
                //data[i++] = t2;
                data[i++] = nodeSize;
                data[i++] = angle

                data[i++] = x2 + c * Math.cos(angle + thita);
                data[i++] = y2 + c * Math.sin(angle + thita);
                //data[i++] = angle + thita;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = c;
                //data[i++] = t2;
                data[i++] = nodeSize;
                data[i++] = angle

                data[i++] = x1 + len * Math.cos(angle + Math.PI / 2);
                data[i++] = y1 + len * Math.sin(angle + Math.PI / 2);
                //data[i++] = angle + Math.PI / 2;
                data[i++] = alpha;
                data[i++] = colorStart;
                //data[i++] = colorEnd;
                //data[i++] = len;
                //data[i++] = t1;
                data[i++] = 0;
                data[i++] = angle
            }

            function addArrowhead(x, y, size, angle, alpha, colorStart, colorEnd, data, nodeSize) {
                //Triangle point1 
                data[i++] = x;
                data[i++] = y;
                //data[i++] = 0;
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = 0;
                //data[i++] = 1;
                data[i++] = nodeSize;
                data[i++] = angle

                //Triangle point 2
                data[i++] = x + size * Math.cos(angle + (Math.PI / 6));
                data[i++] = y + size * Math.sin(angle + (Math.PI / 6));
                //data[i++] = angle + (Math.PI / 6);
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = size;
                //data[i++] = 1;
                data[i++] = nodeSize;
                data[i++] = angle

                //Triangle point 3
                data[i++] = x + size * Math.cos(angle - (Math.PI / 6));
                data[i++] = y + size * Math.sin(angle - (Math.PI / 6));
                //data[i++] = angle - (Math.PI / 6);
                data[i++] = alpha;
                data[i++] = colorEnd;
                //data[i++] = colorEnd;
                //data[i++] = size;
                //data[i++] = 1;
                data[i++] = nodeSize;
                data[i++] = angle
            }

            /** Returns the angle between two points in radians */
            function getAngle(x1, y1, x2, y2) {
                var deltaX = x2 - x1;
                var deltaY = y2 - y1;

                var res = Math.atan2(deltaY, deltaX);

                // /** New faster getAngle implementation */
                // var Xa = 1;
                // var Ya = 0;
                //
                // // var Xb = Math.abs(deltaX);
                // // var Yb = Math.abs(deltaY);
                // var Xb = deltaX;
                // var Yb = deltaY;
                //
                // var dotProd = Xa * Xb + Ya * Yb;
                // var length1 = Math.sqrt(Xa * Xa + Ya * Ya);
                // var length2 = Math.sqrt(Xb * Xb + Yb * Yb);
                //
                // var res = Math.acos(dotProd / (length1 * length2));// * (Xb / deltaX * Yb / deltaY);
                //
                // console.log(res + " " + (res * (180 / Math.PI)) + " - " + Math.cos(Math.abs(res)) * (res/Math.abs(res)));
                return res;
            }

            /** Isolates the alpha value from the color string */
            function getAlpha(color) {
                if (!color) return 1;
                var c = color.split(",");

                var value = (c.length < 4) ? 1 : parseFloat(c[3].replace(")"));

                return value;
            }

            var prevAngle = null;
            var p, xStart, yStart, xEnd, yEnd;

            p = sigma.utils.getPointOnQuadraticCurve(0, x1, y1, x2, y2, cp.x, cp.y);
            xStart = p.x;
            yStart = p.y;

            p = sigma.utils.getPointOnQuadraticCurve(1, x1, y1, x2, y2, cp.x, cp.y);
            xEnd = p.x;
            yEnd = p.y;

            var angle = getAngle(xEnd, yEnd, xStart, yStart);// + (Math.PI / 2.0);
            addArrowhead(xEnd, yEnd, w * 2 * arrowRatio, angle, alpha, colorStart, colorEnd, data, nodeSize);
            addVerts(xStart, yStart, xEnd, yEnd, angle, data, colorStart, colorEnd, alpha, w, 0, 1, nodeSize, arrowRatio);
        },
        render: function (gl, program, data, params) {
            var buffer = g_buffer;
            var update = (!atLocs.colorLocation1);

            // Define attributes:
            if (update) {
                atLocs.colorLocation1 = gl.getAttribLocation(program, 'a_color1');
                //atLocs.colorLocation2 = gl.getAttribLocation(program, 'a_color2');
                atLocs.positionLocation = gl.getAttribLocation(program, 'a_position');
                //atLocs.angleLocation = gl.getAttribLocation(program, 'a_angle');
                atLocs.alphaLocation = gl.getAttribLocation(program, 'a_alpha');
                //atLocs.dirLocation = gl.getAttribLocation(program, 'a_dir');
                //atLocs.tauLocation = gl.getAttribLocation(program, 'a_tau');
                atLocs.nodesizeLocation = gl.getAttribLocation(program, 'a_nodesize');
                atLocs.axisangleLocation = gl.getAttribLocation(program, 'a_axisangle');

                atLocs.resolutionLocation = gl.getUniformLocation(program, 'u_resolution');
                atLocs.matrixLocation = gl.getUniformLocation(program, 'u_matrix');
                atLocs.ratioLocation = gl.getUniformLocation(program, 'u_ratio');
            }

            if (update) {
                buffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

                g_buffer = buffer;
            }
            gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);

            gl.uniform2f(atLocs.resolutionLocation, params.width, params.height);
            gl.uniformMatrix3fv(atLocs.matrixLocation, false, params.matrix);
            gl.uniform1f(atLocs.ratioLocation, Math.pow(params.ratio, params.settings('nodesPowRatio')));

            if (update) {
                gl.enableVertexAttribArray(atLocs.positionLocation);
                gl.enableVertexAttribArray(atLocs.alphaLocation);
                gl.enableVertexAttribArray(atLocs.colorLocation1);
                //gl.enableVertexAttribArray(atLocs.colorLocation2);
                //gl.enableVertexAttribArray(atLocs.dirLocation);
                //gl.enableVertexAttribArray(atLocs.angleLocation);
                //gl.enableVertexAttribArray(atLocs.tauLocation);
                gl.enableVertexAttribArray(atLocs.nodesizeLocation);
                gl.enableVertexAttribArray(atLocs.axisangleLocation);
            }
            gl.vertexAttribPointer(atLocs.positionLocation, 2, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 0);
            //gl.vertexAttribPointer(atLocs.angleLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
            gl.vertexAttribPointer(atLocs.alphaLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
            gl.vertexAttribPointer(atLocs.colorLocation1, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 12);
            //gl.vertexAttribPointer(atLocs.colorLocation2, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 20);
            //gl.vertexAttribPointer(atLocs.dirLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 24);
            //gl.vertexAttribPointer(atLocs.tauLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 28);
            gl.vertexAttribPointer(atLocs.nodesizeLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 16);
            gl.vertexAttribPointer(atLocs.axisangleLocation, 1, gl.FLOAT, false, this.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 20);

            /** By using the following code, we make sure that the shader won't take too much time each time, thus fail and cause restart of driver */
            var start = params.start || 0;
            var end = start + params.count || (data.length / this.ATTRIBUTES);
            /** Each draw, will render 1000 points. This limit was set after testing */
            var step = this.POINTS * 1000;

            for (var i = start; i < end; i += step) {
                var count = Math.min(step, end - i);
                gl.drawArrays(
                    gl.TRIANGLES,
                    i,
                    count
                );
            }
        },
        initProgram: function (gl) {
            var vertexShader,
               fragmentShader,
               program = g_program;
            atLocs = {};
            if (true) {
                vertexShader = sigma.utils.loadShader(
                    gl,
                    [
                        'attribute vec2 a_position;',
                        //'attribute float a_angle;',
                        'attribute float a_alpha;',
                        'attribute float a_color1;',
                        //'attribute float a_color2;',
                        //'attribute float a_dir;',
                        //'attribute float a_tau;',
                        'attribute float a_nodesize;',
                        'attribute float a_axisangle;',

                        'uniform vec2 u_resolution;',
                        'uniform mat3 u_matrix;',
                        'uniform float u_ratio;',

                        'varying vec4 color;',

                        'vec3 interpolateColor(float colorStart, float colorEnd, float _t) {',
                        '	vec3 c, c1, c2;',
                        '	c1.b = mod(colorStart, 256.0); colorStart = floor(colorStart / 256.0);',
                        '	c1.g = mod(colorStart, 256.0); colorStart = floor(colorStart / 256.0);',
                        '	c1.r = mod(colorStart, 256.0);',

                        '	c2.b = mod(colorEnd, 256.0); colorEnd = floor(colorEnd / 256.0);',
                        '	c2.g = mod(colorEnd, 256.0); colorEnd = floor(colorEnd / 256.0);',
                        '	c2.r = mod(colorEnd, 256.0);',

                        '	c.b = (c1.b + (c2.b - c1.b) * _t) / 255.0;',
                        '	c.g = (c1.g + (c2.g - c1.g) * _t) / 255.0;',
                        '	c.r = (c1.r + (c2.r - c1.r) * _t) / 255.0;',

                        '	return c;',
                        '}',

                        'void main() {',
                        // Scale from [[-1 1] [-1 1]] to the container:
                        //'   float angle = a_angle;',
                        '   float x = a_position.x + a_nodesize * u_ratio * cos(a_axisangle);',
                        '   float y = a_position.y + a_nodesize * u_ratio * sin(a_axisangle);',
                        '   vec2 finalPos = vec2(x, y);',
                        '   gl_Position = vec4(',
                        '      ((u_matrix * vec3(finalPos, 1)).xy /',
                        '      u_resolution * 2.0 - 1.0) * vec2(1, -1),',
                        '      0,',
                        '      1',
                        '   );',

                        //'   color = vec4(interpolateColor(a_color1, a_color2, a_tau), a_alpha);',
                        '   float c = a_color1;',
                        '   color.b = mod(c, 256.0); c = floor(c / 256.0);',
                        '   color.g = mod(c, 256.0); c = floor(c / 256.0);',
                        '   color.r = mod(c, 256.0); c = floor(c / 256.0); color /= 255.0;',
                        '   color.a = a_alpha;',
                        '}'
                    ].join('\n'),
                    gl.VERTEX_SHADER
                );

                fragmentShader = sigma.utils.loadShader(
                    gl,
                    [
                        'precision mediump float;',

                        'varying vec4 color;',

                        'void main(void) {',
                        '   gl_FragColor = color;',
                        '}'
                    ].join('\n'),
                    gl.FRAGMENT_SHADER
                );

                program = sigma.utils.loadProgram(gl, [vertexShader, fragmentShader]);

                g_program = program;
            }

            return program;
        }
    };
})();