; (function () {
    'use strict';

    sigma.utils.pkg('sigma.webgl.nodes');

    /**
     * This node renderer will display nodes as stars using WebGL rendering
     *     
     */
    sigma.webgl.nodes.star = {
        glBuffer: null,
        POINTS: 3 * 5 + 10 * 6,
        ATTRIBUTES: 6,
        addNode: function (node, data, i, prefix, settings) {
            var _this = this;

            var x = node[prefix + 'x'];
            var y = node[prefix + 'y'];
            var size = node[prefix + 'size'];
            var color = sigma.utils.floatColor(node.color || settings('defaultNodeColor'));
            var bColor = sigma.utils.floatColor(node.borderColor || settings('defaultNodeColor'));
            var bWidth = node.borderWidth || 0.0;
            var starR = node.starRatio || 0.34392;

            addStar(x, y, size, color, bColor, starR);//3 points
            if (bWidth) {
                var total = -Math.PI / 2;
                var times = 5;
                while (times > 0) {
                    addStarBorder(x, y, size, starR, bColor, total, total - (Math.PI / 5), bWidth); //6 Points
                    addStarBorder(x, y, size, starR, bColor, total, total + (Math.PI / 5), bWidth); //6 Points
                    total += 2 * Math.PI / 5;
                    times--;
                }
            }

            function addStar(x, y, size, color, borderColor, starRatio) {
                var step = (2 * Math.PI) / 5;
                var total = -Math.PI / 2;
                var newX, newY, times = 5;
                while (times > 0) {
                    //Triangle point 1
                    data[i++] = x;
                    data[i++] = y;
                    data[i++] = color;
                    data[i++] = (size * starRatio);
                    data[i++] = total + (Math.PI / 2);
                    data[i++] = 0;

                    //Triangle point 2
                    data[i++] = x;
                    data[i++] = y;
                    data[i++] = color;
                    data[i++] = size;
                    data[i++] = total;
                    data[i++] = 0;

                    //Triangle point 3
                    data[i++] = x;
                    data[i++] = y;
                    data[i++] = color;
                    data[i++] = size * starRatio;
                    data[i++] = total - (Math.PI / 2);
                    data[i++] = 0;

                    total += step;
                    times--;
                }
            }
            function addStarBorder(x, y, size1, starRatio, color, angle1, angle2, borderWidth) {
                //var factor1 = (1 + 1 - Math.sin(angle1)) * 0.5;
                //var factor2 = (1 + 1 - Math.sin(angle2)) * 0.5;
                var factor1 = 0.9;
                var factor2 = 0.85;

                //First Triangle point 1
                data[i++] = x;
                data[i++] = y;
                data[i++] = color;
                data[i++] = size1;
                data[i++] = angle1;
                data[i++] = -borderWidth * factor1 - 0.375;
                //First Triangle point 2
                data[i++] = x;
                data[i++] = y;
                data[i++] = color;
                data[i++] = size1;
                data[i++] = angle1;
                data[i++] = borderWidth * factor1 + 0.375;
                //First Triangle point 3
                data[i++] = x;
                data[i++] = y;
                data[i++] = color;
                data[i++] = size1 * starRatio;
                data[i++] = angle2;
                data[i++] = borderWidth * factor1 * starRatio + 0.375;

                //Second Triangle point 1
                data[i++] = x;
                data[i++] = y;
                data[i++] = color;
                data[i++] = size1 * starRatio;
                data[i++] = angle2;
                data[i++] = borderWidth * factor1 * starRatio + 0.375;
                //Second Triangle point 2
                data[i++] = x;
                data[i++] = y;
                data[i++] = color;
                data[i++] = size1 * starRatio;
                data[i++] = angle2;
                data[i++] = -borderWidth * factor1 * starRatio - 0.375;
                //Second Triangle point 3
                data[i++] = x;
                data[i++] = y;
                data[i++] = color;
                data[i++] = size1;
                data[i++] = angle1;
                data[i++] = -borderWidth * factor1 - 0.375;
            }
            _this.glBuffer = null;
        },
        render: function (gl, program, data, params) {
            var _this = this;
            // Define attributes:
            var positionLocation =
                  gl.getAttribLocation(program, 'a_position'),
                colorLocation =
                  gl.getAttribLocation(program, 'a_color'),
                sizeLocation =
                  gl.getAttribLocation(program, 'a_size'),
                angleLocation =
                  gl.getAttribLocation(program, 'a_angle'),
                borderLocation =
                  gl.getAttribLocation(program, 'a_border'),
                resolutionLocation =
                  gl.getUniformLocation(program, 'u_resolution'),
                matrixLocation =
                  gl.getUniformLocation(program, 'u_matrix'),
                ratioLocation =
                  gl.getUniformLocation(program, 'u_ratio'),
                scaleLocation =
                  gl.getUniformLocation(program, 'u_scale');

            gl.uniform2f(resolutionLocation, params.width, params.height);
            gl.uniform1f(
              ratioLocation, Math.pow(params.ratio, params.settings('nodesPowRatio'))
            );
            gl.uniform1f(scaleLocation, params.scalingRatio);
            gl.uniformMatrix3fv(matrixLocation, false, params.matrix);

            gl.enableVertexAttribArray(positionLocation);
            gl.enableVertexAttribArray(colorLocation);
            gl.enableVertexAttribArray(sizeLocation);
            gl.enableVertexAttribArray(angleLocation);
            gl.enableVertexAttribArray(borderLocation);

            //if (!_this.glBuffer) {
            _this.glBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, _this.glBuffer);
            gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
            //}
            drawBuffer(_this, _this.glBuffer, data)

            //if (!_this.glBorderBuffer) {
            //    _this.glBorderBuffer = gl.createBuffer();
            //    gl.bindBuffer(gl.ARRAY_BUFFER, _this.glBorderBuffer);
            //    gl.bufferData(gl.ARRAY_BUFFER, _this.borderData, gl.STATIC_DRAW);
            //}
            //drawBuffer(_this, _this.glBorderBuffer, _this.borderData)


            function drawBuffer(node, buffer, bufferData) {
                /** By using the following code, we make sure that the shader won't take too much time each time, thus fail and cause restart of driver */
                var start = params.start || 0;
                var end = start + params.count || (bufferData.length / node.ATTRIBUTES);
                /** Each draw, will render 1000 nodes. This limit was set after testing */
                var step = node.POINTS * 100;

                gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
                gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 0);
                gl.vertexAttribPointer(colorLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);
                gl.vertexAttribPointer(sizeLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 12);
                gl.vertexAttribPointer(angleLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 16);
                gl.vertexAttribPointer(borderLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 20);

                //setTimeout(function () {
                for (var i = start; i < end; i += step) {
                    var count = Math.min(step, end - i);
                    gl.drawArrays(gl.TRIANGLES, i, count);
                }
            }
            function drawBufferOld(node, bufferData) {
                var buffer = gl.createBuffer();
                gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
                gl.bufferData(gl.ARRAY_BUFFER, bufferData, gl.DYNAMIC_DRAW);

                gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 0);
                gl.vertexAttribPointer(colorLocation, 1, gl.FLOAT, false, node.ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT, 8);

                /** By using the following code, we make sure that the shader won't take too much time each time, thus fail and cause restart of driver */
                var start = params.start || 0;
                var end = start + params.count || (bufferData.length / node.ATTRIBUTES);
                /** Each draw, will render 1000 nodes. This limit was set after testing */
                var step = node.POINTS * 1000;

                for (var i = start; i < end; i += step) {
                    var count = Math.min(step, end - i);
                    gl.drawArrays(
                        gl.TRIANGLES,
                        i,
                        count
                    );
                }
            }
        },
        initProgram: function (gl) {
            var vertexShader,
                fragmentShader,
                program;

            vertexShader = sigma.utils.loadShader(
              gl,
              [
                'attribute vec2 a_position;',
                'attribute float a_color;',
                'attribute float a_size;',
                'attribute float a_angle;',
                'attribute float a_border;',

                'uniform vec2 u_resolution;',
                'uniform float u_ratio;',
                'uniform float u_scale;',
                'uniform mat3 u_matrix;',

                'varying vec4 color;',

                //'gl_PointSize = a_size * u_ratio * u_scale * 2.0;',
                'vec2 transPoint(vec2 p) {',
                    'float x, y;',
                    'float size = a_size * u_ratio + a_border * u_ratio * u_ratio * 1.1;',
                    'x = p.x + size * cos(a_angle);',
                    'y = p.y + size * sin(a_angle);',
                    'vec2 newP = vec2(x, y);',
                    'return newP;',
                '}',

                'void main() {',
                'vec2 pos =  transPoint(a_position);',
                  // Scale from [[-1 1] [-1 1]] to the container:                  
                  'gl_Position = vec4(',
                    '((u_matrix * vec3(pos, 1)).xy /',
                      'u_resolution * 2.0 - 1.0) * vec2(1, -1),',
                    '0,',
                    '1',
                  ');',

                  //'gl_PointSize = a_size * u_ratio * u_scale * 2.0;',

                  // Extract the color:
                  'float c = a_color;',
                  'color.b = mod(c, 256.0); c = floor(c / 256.0);',
                  'color.g = mod(c, 256.0); c = floor(c / 256.0);',
                  'color.r = mod(c, 256.0); c = floor(c / 256.0); color /= 255.0;',
                  'color.a = 1.0;',
                '}'
              ].join('\n'),
              gl.VERTEX_SHADER
            );

            fragmentShader = sigma.utils.loadShader(
              gl,
              [
                'precision mediump float;',

                'varying vec4 color;',

                'void main(void) {',
                  'float border = 0.01;',
                  'float radius = 0.5;',

                  'vec4 color0 = vec4(0.0, 0.0, 0.0, 0.0);',
                  'vec2 m = gl_PointCoord - vec2(0.5, 0.5);',
                  'float dist = radius - sqrt(m.x * m.x + m.y * m.y);',

                  'float t = 0.0;',
                  'if (dist > border)',
                    't = 1.0;',
                  'else if (dist > 0.0)',
                    't = dist / border;',

                  'gl_FragColor = mix(color0, color, t);',
                '}'
              ].join('\n'),
              gl.FRAGMENT_SHADER
            );

            program = sigma.utils.loadProgram(gl, [vertexShader, fragmentShader]);

            return program;
        }
    };
})();
