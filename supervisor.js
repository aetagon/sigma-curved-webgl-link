;(function(undefined) {
  'use strict';

  if (typeof sigma === 'undefined')
    throw 'sigma is not declared';

  /**
   * Sigma ForceAtlas2.5 Supervisor
   * ===============================
   *
   * Author: Guillaume Plique (Yomguithereal)
   * Version: 0.1
   */
  var _root = this;

  /**
   * Feature detection
   * ------------------
   */
  var webWorkers = 'Worker' in _root;

  /**
   * Supervisor Object
   * ------------------
   */
  function Supervisor(sigInst, options, initial, theta) {
    var _this = this,
        workerFn = sigInst.getForceAtlas2Worker &&
          sigInst.getForceAtlas2Worker();

    options = options || {};

    // _root URL Polyfill
    _root.URL = _root.URL || _root.webkitURL;

    // Properties
    this.sigInst = sigInst;
    this.graph = this.sigInst.graph;
    this.ppn = 10;
    this.ppe = 3;
    this.config = {};
    this.shouldUseWorker =
      options.worker === false ? false : true && webWorkers;
    this.workerUrl = options.workerUrl;
	this.autoConvergence = options.autoConvergence;
	var iterationCounter = 0;

    // State
    this.started = false;
    this.running = false;

    // Web worker or classic DOM events?
    if (this.shouldUseWorker) {
      if (!this.workerUrl) {
        var blob = this.makeBlob(workerFn);
        this.worker = new Worker(URL.createObjectURL(blob));
      }
      else {
        this.worker = new Worker(this.workerUrl);
      }

      // Post Message Polyfill
      this.worker.postMessage =
        this.worker.webkitPostMessage || this.worker.postMessage;
    }
    else {

      eval(workerFn);
    }
	
    // Worker message receiver
    this.msgName = (this.worker) ? 'message' : 'newCoords';
    this.listener = function(e) {
      // Retrieving data
      _this.nodesByteArray = new Float32Array(e.data.nodes);

      // If ForceAtlas2 is running, we act accordingly
      if (_this.running) {

        // Applying layout
        _this.applyLayoutChanges(initial);

        // Send data back to worker and loop
        _this.sendByteArrayToWorker();
		
		var s = _this.sigInst;

		if (initial === true) {
			s.killForceAtlas2();
			sigma.plugins.animate(
				s,
				{
				  x: 'fa2_x',
				  y: 'fa2_y'
				},
				{
				  easing: 'cubicInOut',
				  duration: 1000,
				  onComplete: function() {
					$('#loadingModal').modal('hide');
					$('#layout-dropdown').show();
					$('#layout-li .arrow-left').show();
					if (window.localStorage.doNotShowLayoutPopover !== 'true') {
						$('#run-layout').popover({
							title: 'Stopping the layout',
							content: 'You can stop running the layout using this button. A good moment to stop the layout is when node movement has minimized, which indicates that the layout has stabilized.<div style="text-align:center; margin-top:10px"><label style="margin-bottom:10px" class="checkbox-inline"><input type="checkbox" id="showLayoutPopover"> Do not show this again</label><button id="layoutPopoverClose" type="button" class="btn btn-default">Close <span class="glyphicon glyphicon-remove"></span></button></div>',
							html: true,
							placement: 'right',
							trigger: 'manual',
							container: 'body'
						});
						
						$('#run-layout').popover('show');
							
						$('#layoutPopoverClose').on('click', function(e) {
							$('#run-layout').popover('hide');
						});
						
						$('#showLayoutPopover').on('change', function(e) {
							if (e.target.checked) {
								window.localStorage.doNotShowLayoutPopover = 'true';
							} else {
								window.localStorage.doNotShowLayoutPopover = 'false';
							}
						});
					}

					$('#collapseOne').collapse('show');
					s.refresh();
					s.startForceAtlas2({
					  strongGravityMode: true,
					  gravity: 0.1,
					  worker: true,
					  workerUrl: 'worker.js',
					  scalingRatio: 1,
					  slowDown: 3,
					  barnesHutOptimize: true,
					  barnesHutTheta: theta,
					}, false);
				  }
				}
			  );
		}
		
		// Rendering graph
        _this.sigInst.refresh();
      }
    };

    (this.worker || document).addEventListener(this.msgName, this.listener);

    // Filling byteArrays
    this.graphToByteArrays();

    // Binding on kill to properly terminate layout when parent is killed
    sigInst.bind('kill', function() {
      sigInst.killForceAtlas2();
    });
  }

  Supervisor.prototype.makeBlob = function(workerFn) {
    var blob;

    try {
      blob = new Blob([workerFn], {type: 'application/javascript'});
    }
    catch (e) {
      _root.BlobBuilder = _root.BlobBuilder ||
                          _root.WebKitBlobBuilder ||
                          _root.MozBlobBuilder;

      blob = new BlobBuilder();
      blob.append(workerFn);
      blob = blob.getBlob();
    }

    return blob;
  };

  Supervisor.prototype.graphToByteArrays = function() {
    var nodes = this.graph.nodes().filter(function(n) {return !n.hidden}),
        edges = this.graph.edges().filter(function(e) {return !e.hidden}),
        nbytes = nodes.length * this.ppn,
        ebytes = edges.length * this.ppe,
        nIndex = {},
        i,
        j,
        l;

    // Allocating Byte arrays with correct nb of bytes
    this.nodesByteArray = new Float32Array(nbytes);
    this.edgesByteArray = new Float32Array(ebytes);

    // Iterate through nodes
    for (i = j = 0, l = nodes.length; i < l; i++) {

      // Populating index
      nIndex[nodes[i].id] = j;

      // Populating byte array
      this.nodesByteArray[j] = nodes[i].x;
      this.nodesByteArray[j + 1] = nodes[i].y;
      this.nodesByteArray[j + 2] = 0;
      this.nodesByteArray[j + 3] = 0;
      this.nodesByteArray[j + 4] = 0;
      this.nodesByteArray[j + 5] = 0;
      this.nodesByteArray[j + 6] = 1 + this.graph.degree(nodes[i].id);
      this.nodesByteArray[j + 7] = 1;
      this.nodesByteArray[j + 8] = nodes[i].size;
      this.nodesByteArray[j + 9] = 0;
      j += this.ppn;
    }

    // Iterate through edges
    for (i = j = 0, l = edges.length; i < l; i++) {
      this.edgesByteArray[j] = nIndex[edges[i].source];
      this.edgesByteArray[j + 1] = nIndex[edges[i].target];
      this.edgesByteArray[j + 2] = edges[i].weight || 0;
      j += this.ppe;
    }
  };

  // TODO: make a better send function
  Supervisor.prototype.applyLayoutChanges = function(initial) {
    var nodes = this.graph.nodes().filter(function(n) {return !n.hidden}),
        j = 0,
        realIndex;

    // Moving nodes
	var sumDeltaX = 0, sumDeltaY = 0;
    for (var i = 0, l = this.nodesByteArray.length; i < l; i += this.ppn) {
	  if (initial) {
		  nodes[j].fa2_x = this.nodesByteArray[i];
		  nodes[j].fa2_y = this.nodesByteArray[i + 1];
		  j++;
	  } else {
		  if (!nodes[j].pinned) {
			  nodes[j].previousX = nodes[j].x;
			  nodes[j].previousY = nodes[j].y;
			  nodes[j].x = this.nodesByteArray[i];
			  nodes[j].y = this.nodesByteArray[i + 1];
			  nodes[j].deltaX = nodes[j].previousX - nodes[j].x;
			  nodes[j].deltaY = nodes[j].previousY - nodes[j].y;
			  sumDeltaX += nodes[j].deltaX;
			  sumDeltaY += nodes[j].deltaY;

			  //this.graph.updateNodes(nodes[j].id);
			  nodes[j].needsUpdate = true;
		  }
		  j++;
	  }
    }
	
	if (this.autoConvergence) {
		var averageDeltaX = sumDeltaX / this.nodesByteArray.length;
		var averageDeltaY = sumDeltaY / this.nodesByteArray.length;
		if (averageDeltaX < 0) {
			averageDeltaX *= -1;
		}
		if (averageDeltaY < 0) {
			averageDeltaY *= -1;
		}
		var averageOverallDelta = (averageDeltaX + averageDeltaY) / 2;
		var convergence = 0.001;
		if (averageOverallDelta < convergence) {
			$('#run-layout').click();
		}	
	}
  };

  Supervisor.prototype.sendByteArrayToWorker = function(action) {
    var content = {
      action: action || 'loop',
      nodes: this.nodesByteArray.buffer
    };

    var buffers = [this.nodesByteArray.buffer];

    if (action === 'start') {
      content.config = this.config || {};
      content.edges = this.edgesByteArray.buffer;
      buffers.push(this.edgesByteArray.buffer);
    }
	
	if (this.isIE = navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
		if (this.shouldUseWorker)
		  this.worker.postMessage(content);
		else
		  _root.postMessage(content, '*');
	} else {
		if (this.shouldUseWorker)
		  this.worker.postMessage(content, buffers);
		else
		  _root.postMessage(content, '*');
	}
  };

  Supervisor.prototype.start = function() {
    if (this.running)
      return;

    this.running = true;

    // Do not refresh edgequadtree during layout:
    var k,
        c;
    for (k in this.sigInst.cameras) {
      c = this.sigInst.cameras[k];
      c.edgequadtree._enabled = false;
    }

    if (!this.started) {

      // Sending init message to worker
      this.sendByteArrayToWorker('start');
      this.started = true;
    }
    else {
      this.sendByteArrayToWorker();
    }
  };

  Supervisor.prototype.stop = function() {
    if (!this.running)
      return;

    // Allow to refresh edgequadtree:
    var k,
        c,
        bounds;
    for (k in this.sigInst.cameras) {
      c = this.sigInst.cameras[k];
      c.edgequadtree._enabled = true;

      // Find graph boundaries:
      bounds = sigma.utils.getBoundaries(
        this.graph,
        c.readPrefix
      );

      // Refresh edgequadtree:
      if (c.settings('drawEdges') && c.settings('enableEdgeHovering'))
        c.edgequadtree.index(this.sigInst.graph, {
          prefix: c.readPrefix,
          bounds: {
            x: bounds.minX,
            y: bounds.minY,
            width: bounds.maxX - bounds.minX,
            height: bounds.maxY - bounds.minY
          }
        });
    }

    this.running = false;
  };

  Supervisor.prototype.killWorker = function() {
    if (this.worker) {
      this.worker.terminate();
    }
    else {
      _root.postMessage({action: 'kill'}, '*');
      document.removeEventListener(this.msgName, this.listener);
    }
  };

  Supervisor.prototype.configure = function(config) {

    // Setting configuration
    this.config = config;

    if (!this.started)
      return;

    var data = {action: 'config', config: this.config};

    if (this.shouldUseWorker)
      this.worker.postMessage(data);
    else
      _root.postMessage(data, '*');
  };

  /**
   * Interface
   * ----------
   */
  sigma.prototype.startForceAtlas2 = function(config, initial, theta) {

    // Create supervisor if undefined
    if (!this.supervisor)
      this.supervisor = new Supervisor(this, config, initial, theta);

    // Configuration provided?
    if (config)
      this.supervisor.configure(config);

    // Start algorithm
    this.supervisor.start();

    return this;
  };

  sigma.prototype.stopForceAtlas2 = function() {
    if (!this.supervisor)
      return this;

    // Pause algorithm
    this.supervisor.stop();

    return this;
  };

  sigma.prototype.killForceAtlas2 = function() {
    if (!this.supervisor)
      return this;

    // Stop Algorithm
    this.supervisor.stop();

    // Kill Worker
    this.supervisor.killWorker();

    // Kill supervisor
    this.supervisor = null;

    return this;
  };

  sigma.prototype.configForceAtlas2 = function(config) {
    if (!this.supervisor)
      this.supervisor = new Supervisor(this, config);

    this.supervisor.configure(config);

    return this;
  };

  sigma.prototype.isForceAtlas2Running = function(config) {
    return !!this.supervisor && this.supervisor.running;
  };
}).call(this);